(ns jdbc.assistant.postgresql
  (:require [jdbc.assistant.core :as assistant]
            [jdbc-hodur.metadata.query :as mdq]
            [edn-query-language.core :as eql]
            [clojure.java.jdbc :as jdbc]
            [clojure.string :as str]
            [honeysql.core :as sql]
            [honeysql.helpers :as sql-helpers]
            [honeysql.format :as fmt]
            [cheshire.core :as json]))

(defmethod ^{:private true} fmt/format-clause :left-join-lateral [[_ join-groups] _]
  (clojure.string/join
   " "
   (map (fn [[join-group alias]]
          (str "LEFT JOIN LATERAL " (fmt/to-sql join-group) " AS " (name alias) " ON TRUE"))
        join-groups)))

(defmethod ^{:private true} fmt/format-clause :coalesce-array [[_ x] _]
  (str " COALESCE (" (fmt/to-sql x) ",'[]')"))

(defmethod ^{:private true} fmt/format-clause :coalesce-array [[_ x] _]
  (str " COALESCE (" (fmt/to-sql x) ",'[]')"))

(fmt/register-clause! :left-join-lateral 135)

(defn- row-to-json [column-name]
  (keyword (str "%row_to_json." (name column-name))))

(defn- json-agg [select-alias]
  (keyword (str "%json_agg." select-alias ".*")))

(defn- result-set-hql [hql]
  {:with [[:rs hql]]
   :from [[{:select [:*]
            :from [:rs]} :rs]]
   :select [(sql/raw "coalesce (json_agg(\"rs\"), '[]')::character varying as result")]})

(defn- execute-query [db-spec hql]
  (-> (jdbc/query db-spec (sql/format (result-set-hql hql) :quoting :ansi))
      first
      :result
      (json/parse-string true)))

(defn- join-relationship [hodur-schema key]
  (:jdbc.metadata.column/relationship (mdq/column hodur-schema key)))

(defn- resolve-join-type [hodur-schema key]
  (if (coll? key)
    :join-by-ident
    (case (join-relationship hodur-schema key)
      :one-to-one :join-one-to-one
      :one-to-many :join-one-to-many)))

(defmulti ^{:private true} eql-node-to-hql
  (fn [hodur-schema {:keys [type key]}]
    (if (= type :join)
      (resolve-join-type hodur-schema key)
      type)))

(defn- one-to-one-select-alias [{:jdbc.metadata.column/keys [fk-table-name] :as hodur-column}]
  (str fk-table-name "_" (:field/name hodur-column)))

(defn- hql-column-name [table-schema table-name name]
  (keyword (str/join "." [table-schema table-name name])))

(defn- ident-column [hodur-schema ident]
  (let [{:jdbc.metadata.column/keys [table-schema table-name name]} (mdq/column hodur-schema ident)]
    (hql-column-name table-schema table-name name)))

(defn- ident-key->hql-predicate [hodur-schema key]
  (let [ident-predicates (map (fn [[i v]]
                                [:= (ident-column hodur-schema i) v]) (partition 2 key))]
    (if (= 1 (count ident-predicates))
      (first ident-predicates)
      (conj ident-predicates :and))))

(defmethod ^{:private true} eql-node-to-hql :root [hodur-schema {:keys [children]}]
  (eql-node-to-hql hodur-schema (first children)))

(defn- filter-join-children [relationship hodur-schema children]
  (filter #(= relationship (join-relationship hodur-schema (:key %)))
          (filter #(= :join (:type %)) children)))

(defn- hodur-column->hql-table [{:jdbc.metadata.column/keys [table-schema table-name]}]
  (keyword (str/join "." [table-schema table-name])))

(defn- hodur-column->hql-column [{:jdbc.metadata.column/keys [table-schema table-name name relationship]
                                  :as hodur-column}]
  (let [field-camelCaseName (:field/camelCaseName hodur-column)]
    (case relationship
      :one-to-one [(row-to-json (str (one-to-one-select-alias hodur-column) ".*"))
                   field-camelCaseName]
      :one-to-many nil
      [(hql-column-name table-schema table-name name)
       field-camelCaseName])))

(defn- hodur-columns->hql-columns [columns]
  (filter some? (map hodur-column->hql-column columns)))

(defn- merge-selects [hql selects]
  (reduce #(sql-helpers/merge-select %1 %2) hql selects))

(defmethod ^{:private true} eql-node-to-hql :join-by-ident [hodur-schema {:keys [children key]}]
  (let [columns (mdq/columns hodur-schema (map :key children))
        hql-columns (hodur-columns->hql-columns columns)
        left-join-children (filter-join-children :one-to-one hodur-schema children)
        one-to-many-children (filter-join-children :one-to-many hodur-schema children)
        hql {:select hql-columns
             :from [(hodur-column->hql-table (first columns))]
             :where (ident-key->hql-predicate hodur-schema key)}]
    (cond-> hql
      (seq left-join-children) (assoc :left-join-lateral
                                      (map #(eql-node-to-hql hodur-schema %) left-join-children))
      (seq one-to-many-children) (merge-selects (map #(eql-node-to-hql hodur-schema %) one-to-many-children)))))

(defn- one-to-any-join-predicate [hodur-column]
  (let [{:jdbc.metadata.column/keys [table-schema table-name name
                                     fk-table-schema fk-table-name fk-column-name]} hodur-column]
    [:=
     (hql-column-name fk-table-schema fk-table-name fk-column-name)
     (hql-column-name table-schema table-name name)]))

(defmethod ^{:private true} eql-node-to-hql :join-one-to-one [hodur-schema {:keys [children key]}]
  (let [columns (mdq/columns hodur-schema (map :key children))
        hql-columns (hodur-columns->hql-columns columns)
        join-hodur-column (mdq/column hodur-schema key)
        alias (keyword (one-to-one-select-alias join-hodur-column))
        left-join-children (filter-join-children :one-to-one hodur-schema children)
        one-to-many-children (filter-join-children :one-to-many hodur-schema children)
        hql {:select (concat hql-columns (map #(eql-node-to-hql hodur-schema %) one-to-many-children))
             :from [(hodur-column->hql-table (first columns))]
             :where (one-to-any-join-predicate join-hodur-column)}]
    [(assoc hql :left-join-lateral (map #(eql-node-to-hql hodur-schema %) left-join-children))
     alias]))

(defmethod ^{:private true} eql-node-to-hql :join-one-to-many [hodur-schema {:keys [children key]}]
  (let [columns (mdq/columns hodur-schema (map :key children))
        hql-columns (hodur-columns->hql-columns columns)
        join-hodur-column (mdq/column hodur-schema key)
        one-to-many-children (filter-join-children :one-to-many hodur-schema children)
        left-join-children (filter-join-children :one-to-one hodur-schema children)
        alias (:field/camelCaseName join-hodur-column)
        inner-hql {:select (concat hql-columns (map #(eql-node-to-hql hodur-schema %) one-to-many-children))
                   :from [(hodur-column->hql-table (first columns))]
                   :where (one-to-any-join-predicate join-hodur-column)}
        hql (if (seq left-join-children)
              (assoc inner-hql :left-join-lateral
                     (map #(eql-node-to-hql hodur-schema %) left-join-children))
              inner-hql)]
    [{:coalesce-array {:select [(json-agg (name alias))]
                       :from [[hql alias]]}}
     alias]))

(defn- query-single [db-spec hodur-schema q]
  (let [hql (eql-node-to-hql hodur-schema (eql/query->ast q))]
    (first (execute-query db-spec hql))))

(defrecord Postgresql [db-spec]
  assistant/JdbcAssistable
  (query-single [_ hodur-schema q]
    (query-single db-spec hodur-schema q)))

(comment
  (require '[jdbc-hodur.core :as jh]
           '[clojure.jdbc.metadata :as m]
           '[datascript.core :as d])
  (def q [{[:Payment/paymentId 20001] [{:Payment/customer [:Customer/firstName :Customer/lastName {:Customer/rentals [:Rental/rentalDate]}]}]}])
  (def db-spec {:dbtype "postgres"
                :dbname "dvdrental"
                :user "postgres"
                :password "postgres"})
  (def db-metadata (m/fetch db-spec))
  (d/q '[:find (pull ?c [*])
         :where         [?c :field/name "payments"]] @hodur-schema)
  (def hodur-schema (jh/hodur-schema {} db-metadata))
  (mdq/column hodur-schema :Language/films)
  (eql-node-to-hql hodur-schema (eql/query->ast q))
  (query-single db-spec hodur-schema q))