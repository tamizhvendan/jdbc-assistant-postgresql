(ns jdbc.assistant.query-test
  (:require [jdbc.assistant.core :as assistant]
            [clojure.jdbc.metadata :as metadata]
            [jdbc.assistant.postgresql :as pg]
            [clojure.test :refer [deftest is testing]]
            [jdbc-hodur.core :as jh]))

(def db-spec {:dbtype "postgres"
              :dbname "dvdrental"
              :user "postgres"
              :password "postgres"})
(def pg-inst (pg/->Postgresql db-spec))
(def db-metadata (metadata/fetch db-spec))
(def hodur-schema (jh/hodur-schema {} db-metadata))

(deftest select-a-table-columns-by-its-primary-key
  (testing "SELECT c1, c2 FROM t1 WHERE pk1 = x1"
    (let [q [{[:Film/filmId 2] [:Film/title :Film/releaseYear]}]
          expected {:releaseYear 2006 :title "Ace Goldfinger"}]
      (is (= expected (assistant/query-single pg-inst hodur-schema q))))))

(deftest select-a-table-columns-by-its-primary-keys
  (testing "SELECT c1 FROM t1 WHERE pk1 = x1 AND pk2 = x2"
    (let [q [{[:FilmActor/filmId 1 :FilmActor/actorId 1] [:FilmActor/lastUpdate]}]
          expected {:lastUpdate "2006-02-15T10:05:03"}]
      (is (= expected (assistant/query-single pg-inst hodur-schema q))))
    (let [q [{[:FilmActor/filmId 1 :FilmActor/actorId 1000] [:FilmActor/lastUpdate]}]
          expected nil]
      (is (= expected (assistant/query-single pg-inst hodur-schema q))))))

(deftest select-a-table-columns-with-one-to-one-relationship
  (testing "SELECT t1.c1, t2.c2 FROM t1 LEFT JOIN t2 ON t1.id = t2.id
            WHERE t1.pk1 = x1"
    (let [q [{[:City/cityId 1] [:City/city {:City/country [:Country/country]}]}]
          expected {:city "A Corua (La Corua)"
                    :country {:country "Spain"}}]
      (is (= expected (assistant/query-single pg-inst hodur-schema q))))))

(deftest select-table-columns-with-nested-one-to-one-relationship
  (testing "customer -> address -> city -> country"
    (let [q [{[:Customer/customerId 1]
              [:Customer/firstName
               :Customer/lastName
               {:Customer/address
                [:Address/address
                 :Address/address2
                 {:Address/city
                  [:City/city
                   {:City/country
                    [:Country/country]}]}]}]}]
          expected {:firstName "Mary"
                    :lastName "Smith"
                    :address {:address "1913 Hanoi Way"
                              :address2 ""
                              :city {:city "Sasebo"
                                     :country {:country "Japan"}}}}]
      (is (= expected (assistant/query-single pg-inst hodur-schema q))))))

(def customer-318
  {:payments
   [{:amount 2.99, :paymentDate "2007-03-21T05:02:31.996577"} {:amount 4.99, :paymentDate "2007-04-06T10:02:03.996577"}
    {:amount 2.99, :paymentDate "2007-04-06T21:27:42.996577"} {:amount 8.99, :paymentDate "2007-04-07T17:49:48.996577"}
    {:amount 0.99, :paymentDate "2007-04-28T00:05:52.996577"} {:amount 0.99, :paymentDate "2007-04-28T08:05:04.996577"}
    {:amount 5.99, :paymentDate "2007-04-30T16:54:17.996577"}]
   :firstName "Brian"
   :lastName "Wyman"
   :rentals [{:rentalDate "2005-05-26T10:18:27"} {:rentalDate "2005-06-19T08:55:17"} {:rentalDate "2005-06-19T09:39:27"} {:rentalDate "2005-06-21T10:24:35"}
             {:rentalDate "2005-06-21T13:43:02"} {:rentalDate "2005-07-06T11:33:37"} {:rentalDate "2005-07-06T22:59:16"} {:rentalDate "2005-07-07T19:21:22"}
             {:rentalDate "2005-07-28T01:37:26"} {:rentalDate "2005-07-28T09:36:38"} {:rentalDate "2005-07-31T18:25:51"} {:rentalDate "2005-08-21T06:34:05"}]})

(deftest select-table-columns-with-one-to-many-relationships
  (testing "customer -> payments"
    (let [{:keys [firstName lastName payments]} customer-318
          q [{[:Customer/customerId 318] [:Customer/firstName :Customer/lastName {:Customer/payments [:Payment/paymentDate :Payment/amount]}]}]
          expected {:lastName lastName, :firstName firstName, :payments payments}]
      (is (= expected (assistant/query-single pg-inst hodur-schema q))))))

(deftest nested-one-to-many-relationships
  (testing "country -> cities -> addresses"
    (let [q [{[:Country/countryId 8] [:Country/country {:Country/cities [:City/city {:City/addresses [:Address/address]}]}]}]
          expected {:country "Australia", :cities [{:city "Woodridge", :addresses [{:address "28 MySQL Boulevard"}
                                                                                   {:address "1411 Lillydale Drive"}]}]}]
      (is (= expected (assistant/query-single pg-inst hodur-schema q))))))

(deftest multiple-columns-with-one-to-many-relationships
  (testing "customer -> payments, rentals"
    (let [{:keys [firstName lastName payments rentals]} customer-318
          q [{[:Customer/customerId 318] [:Customer/firstName :Customer/lastName
                                          {:Customer/payments [:Payment/paymentDate :Payment/amount]}
                                          {:Customer/rentals [:Rental/rentalDate]}]}]
          expected {:lastName lastName, :firstName firstName, :payments payments :rentals rentals}]
      (is (= expected (assistant/query-single pg-inst hodur-schema q))))))

(deftest one-to-one-inside-one-to-many
  (testing "language -> films -> language"
    (let [q [{[:Language/languageId 2] [{:Language/films [:Film/filmId :Film/title {:Film/language [:Language/name]}]}]}]
          expected {:films [{:title "Curtain Videotape", :filmId 200, :language {:name "Italian             "}}]}]
      (is (= expected (assistant/query-single pg-inst hodur-schema q))))))

(deftest one-to-many-inside-one-to-one
  (testing "payment -> customer -> rentals"
    (let [q [{[:Payment/paymentId 20001] [{:Payment/customer [:Customer/firstName :Customer/lastName {:Customer/rentals [:Rental/rentalDate]}]}]}]
          {:keys [firstName lastName rentals]} customer-318
          expected {:customer {:lastName lastName :firstName firstName :rentals rentals}}]
      (is (= expected (assistant/query-single pg-inst hodur-schema q))))))