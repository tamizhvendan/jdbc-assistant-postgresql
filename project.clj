(defproject jdbc.assistant.postgresql "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.10.0"]
                 [jdbc.assistant.core "0.1.0-SNAPSHOT"]
                 [jdbc-hodur "0.1.0-SNAPSHOT"]
                 [org.clojure/java.jdbc "0.7.9"]
                 [edn-query-language/eql "0.0.3"]
                 [honeysql "0.9.4"]
                 [cheshire "5.8.1"]]
  :profiles {:test {:dependencies [[clojure.jdbc.metadata "0.1.0-SNAPSHOT"]
                                   [org.postgresql/postgresql "42.2.5"]]}})
